const wdk = require('wikidata-sdk');
const {username, password} = require('./credentials')
const wde = require('wikidata-edit')({username, password})
const fetch = require('node-fetch');
const bot = require('nodemw');
const client = new bot({
    protocol: 'https',
    server: 'www.wikidata.org',
    path: '/w',                  // path to api.php script
    debug: false,                 // is more verbose when set to true
    username,
    password
  });
const qs = require('querystring')

const SANDBOX = 'Q4115189'

const propertyLabelToID = async (name) => {
   const qry = `SELECT ?property
      WHERE {
         ?property wikibase:propertyType ?propertyType ;
         rdfs:label ?propertyLabel.
         FILTER(LANG(?propertyLabel) = "en").
         FILTER(STRSTARTS(?propertyLabel, "${name}")).
         FILTER(STRLEN(?propertyLabel) = ${name.length}).
      }`
    const url = wdk.sparqlQuery(qry)
    const resp = await fetch(url)
    const json = await resp.json()
    const arr = wdk.simplify.sparqlResults(json)
    const ids = arr.map((item) => item.property)
    if (ids.length > 1) {
      throw new Error(`Too many properties with label ${name}`)
    } else if (ids.length === 0) {
      return null
    } else {
      return ids[0]
    }
}

const propertyAliasToID = async (name) => {
   const qry = `SELECT ?property
      WHERE {
         ?property wikibase:propertyType ?propertyType ;
         skos:altLabel ?propertyAlias.
         FILTER(LANG(?propertyAlias) = "en").
         FILTER(STRSTARTS(?propertyAlias, "${name}")).
         FILTER(STRLEN(?propertyAlias) = ${name.length}).
      }`
    const url = wdk.sparqlQuery(qry)
    const resp = await fetch(url)
    const json = await resp.json()
    const arr = wdk.simplify.sparqlResults(json)
    const ids = arr.map((item) => item.property)
    if (ids.length > 1) {
      throw new Error(`Too many properties with alias ${name}`)
    } else if (ids.length === 0) {
      return null
    } else {
      return ids[0]
    }
}

const propertyNameToID = async (name) => {
  let id = await propertyLabelToID(name)
  if (!id) {
    id = await propertyAliasToID(name)
  }
  return id
}

const isID = (item) => {
    return typeof(item) === 'string' && item.match(/^Q\d+$/)
}

const entityNameToID = async (name) => {
    let sname = name.replace(/^the /, '')
    const args = qs.stringify({action: "wbsearchentities", language: "en", search: sname, format: 'json'})
    const url = `https://www.wikidata.org/w/api.php?${args}`
    const resp = await fetch(url)
    const json = await resp.json()
    if (!json.search || json.search.length === 0) {
      return null
    }
    const id = json.search[0].id
    const eurl = wdk.getEntities([id])
    const eresp = await fetch(eurl)
    const ejson = await eresp.json()
    const map = wdk.simplify.entities(ejson.entities)
    const keys = Object.keys(map)
    if (keys.length === 0) {
        return null
    } else {
        const key = keys[0]
        if (!isID(key)) {
            return null
        } else {
            return map[key]
        }
    }
}

const getEntityLabels = async (ids) => {
    const url = wdk.getEntities(ids)
    const resp = await fetch(url)
    const json = await resp.json()
    const map = wdk.simplify.entities(json.entities)
    const labels = Object.keys(map).map(id => map[id].labels.en)
    return labels
}

const valuesOf = async (pid, ent) => {
    if (!ent.claims) {
        return []
    }

    if (!ent.claims[pid]) {
      return []
    }

    const values = ent.claims[pid]

    if (values.every(isID)) {
      const labels = await getEntityLabels(values)
      return labels
    } else {
      return values
    }
}

const hasValue = async (pid, ent, vent) => {
    // const entityID = ent.id
    const entityID = SANDBOX
    return wde.claim.exists(entityID, pid, vent.id)
}

const setValue = async (pid, ent, vent) => {
    // const entityID = ent.id
    const entityID = SANDBOX
    return wde.claim.add(entityID, pid, vent.id)
}

const addTalkPageComment = (entityID, title, text) => {
    return new Promise((resolve, reject) => {
        const content = `== ${title} ==\n\n${text} ~~~~`
        client.append(`Talk:${entityID}`, content, `Automated comment from experimental voice bot`, (err) => {
            if (err) {
                reject(err)
            } else {
                resolve()
            }
        })
    })
}

const registerDoubt = async (entityID, propertyName) => {
    const title = `Problem with ${propertyName}`
    const text = `A user connected by voice expressed doubt about the accuracy of the ${propertyName} claim for this entity. It may be worth reviewing.`
    const results = await addTalkPageComment(entityID, title, text)
    return
}

module.exports = { propertyNameToID, entityNameToID, valuesOf, hasValue, setValue, registerDoubt}
