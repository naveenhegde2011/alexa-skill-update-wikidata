const Alexa = require('ask-sdk-core');

const { propertyNameToID, entityNameToID, valuesOf, hasValue, setValue, registerDoubt } = require('./wikidata');

const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
    },
    handle(handlerInput) {
        const speechText = 'Ask a question or make an update';
        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .getResponse();
    }
};
const QueryIntentHandler  = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'QueryIntent';
    },
    async handle(handlerInput) {
        let speechText = "I don't know"
        const propertyOfEntity = handlerInput.requestEnvelope.request.intent.slots.propertyOfEntity.value;
        const [, property, entity] = propertyOfEntity.match(/^(.*?)\s+of\s+(.*?)$/)
        const [pid, ent] = await Promise.all([propertyNameToID(property), entityNameToID(entity)])

        if (!pid) {
            speechText = `I don't know what ${property} means.`
            return handlerInput.responseBuilder
                .speak(speechText)
                .withShouldEndSession(false)
                .getResponse();
        }

        if (!ent) {
             speechText = `I don't have any information about ${entity}.`
            return handlerInput.responseBuilder
                .speak(speechText)
                .withShouldEndSession(false)
                .getResponse();
        }

        const values = await valuesOf(pid, ent)

        if (values.length === 0) {
             speechText = `I don't know about the ${property} of ${entity}.`
            return handlerInput.responseBuilder
                .speak(speechText)
                .withShouldEndSession(false)
                .getResponse();
        }

        const attrs = handlerInput.attributesManager.getSessionAttributes()

        attrs.property = property
        attrs.entityID = ent.id

        handlerInput.attributesManager.setSessionAttributes(attrs)

        speechText = `The ${property} of ${entity} is ${values.join(' and ')}`;
        return handlerInput.responseBuilder
            .speak(speechText)
            .withShouldEndSession(false)
            .getResponse();
    }
};
const UpdateIntentHandler  = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'UpdateIntent';
    },
    async handle(handlerInput) {
        let speechText = "I don't know"
        const propertyOfEntityIsValue = handlerInput.requestEnvelope.request.intent.slots.propertyOfEntityIsValue.value;
        const [, property, entity, value] = propertyOfEntityIsValue.match(/^(.*?)\s+of\s+(.*?)\s+is\s+(.*)$/);
        const [pid, ent, vent] = await Promise.all([propertyNameToID(property), entityNameToID(entity), entityNameToID(value)])

        if (!pid) {
            speechText = `I don't know what ${property} means.`
            return handlerInput.responseBuilder
                .speak(speechText)
                .withShouldEndSession(false)
                .getResponse();
        }

        if (!ent) {
             speechText = `I don't have any information about ${entity}.`
            return handlerInput.responseBuilder
                .speak(speechText)
                .withShouldEndSession(false)
                .getResponse();
        }

        if (!vent) {
            speechText = `I don't know about the value ${value}.`
            return handlerInput.responseBuilder
                .speak(speechText)
                .withShouldEndSession(false)
                .getResponse();
        }

        const exists = await hasValue(pid, ent, vent)

        if (exists) {
            speechText = `Yes, I knew that already.`
            return handlerInput.responseBuilder
                .speak(speechText)
                .withShouldEndSession(false)
                .getResponse();
        }

        const done = await setValue(pid, ent, vent)

        speechText = `OK, I'll remember that.`
        return handlerInput.responseBuilder
            .speak(speechText)
            .withShouldEndSession(false)
            .getResponse();
    }
};

const DoubtIntentHandler  = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'DoubtIntent';
    },
    async handle(handlerInput) {
        let speechText = null
        const attrs = handlerInput.attributesManager.getSessionAttributes()

        if (!attrs.entityID) {
            speechText = `I don't know what you're talking about.`
            return handlerInput.responseBuilder
                .speak(speechText)
                .withShouldEndSession(false)
                .getResponse();
        }

        if (!attrs.property) {
            speechText = `I forgot what property we're talking about.`
            return handlerInput.responseBuilder
                .speak(speechText)
                .withShouldEndSession(false)
                .getResponse();
        }

        // const results = await registerDoubt(attrs.entityID, attrs.property)

        speechText = `Thanks for letting me know. I've made a note of it and will review soon.`

        return handlerInput.responseBuilder
            .speak(speechText)
            .withShouldEndSession(false)
            .getResponse();
    }
};

const HelpIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.HelpIntent';
    },
    handle(handlerInput) {
        const speechText = 'Ask a question to get the property of an item. Make an update to change the property of something.';

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .getResponse();
    }
};
const CancelAndStopIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent'
                || handlerInput.requestEnvelope.request.intent.name === 'AMAZON.StopIntent');
    },
    handle(handlerInput) {
        const speechText = 'Goodbye!';
        return handlerInput.responseBuilder
            .speak(speechText)
            .getResponse();
    }
};
const SessionEndedRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        // Any cleanup logic goes here.
        return handlerInput.responseBuilder.getResponse();
    }
};

// The intent reflector is used for interaction model testing and debugging.
// It will simply repeat the intent the user said. You can create custom handlers
// for your intents by defining them above, then also adding them to the request
// handler chain below.
const IntentReflectorHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest';
    },
    handle(handlerInput) {
        const intentName = handlerInput.requestEnvelope.request.intent.name;
        const speechText = `You just triggered ${intentName}`;

        return handlerInput.responseBuilder
            .speak(speechText)
            //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
            .getResponse();
    }
};

// Generic error handling to capture any syntax or routing errors. If you receive an error
// stating the request handler chain is not found, you have not implemented a handler for
// the intent being invoked or included it in the skill builder below.
const ErrorHandler = {
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        console.log(`~~~~ Error handled: ${error.message}`);
        const speechText = `Sorry, I couldn't understand what you said. Please try again.`;

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .getResponse();
    }
};

// This handler acts as the entry point for your skill, routing all request and response
// payloads to the handlers above. Make sure any new handlers or interceptors you've
// defined are included below. The order matters - they're processed top to bottom.
exports.handler = Alexa.SkillBuilders.custom()
    .addRequestHandlers(
        LaunchRequestHandler,
        QueryIntentHandler,
        UpdateIntentHandler,
        DoubtIntentHandler,
        HelpIntentHandler,
        CancelAndStopIntentHandler,
        SessionEndedRequestHandler,
        IntentReflectorHandler) // make sure IntentReflectorHandler is last so it doesn't override your custom intent handlers
    .addErrorHandlers(
        ErrorHandler)
    .lambda();
